import React from 'react';

const routes=[];

//ACCOUNTING
const IndexPage = React.lazy(() => import('./JS/Pages/IndexPage'));
const GridSystem = React.lazy(() => import('./JS/Pages/GridSystem'));
routes.push(
    { path: '/grid-system/',  exact: true, name: 'GridSystem', component: GridSystem },
    { path: '/',  name: 'IndexPage', component: IndexPage },
);

export default routes;