import React from 'react';
import reactElementToJSXString from 'react-element-to-jsx-string';

const GridSystem = () => {
    return (
        <div className="content">
            <h1>Bootstrap 4 grid-system addon</h1>
            <h2>Tiles</h2>
            <p>You can do grid width vertical columns paddings as horizontal</p>
            <figure>
                <figcaption>Just add class tiles to container</figcaption>
                <pre>
                    <code>
                        {reactElementToJSXString(
                            <div class="container tiles">
                                <div class="row">
                                    <div class="col-12 col-lg-3">
                                        <div class="column-inner j5"> column inner</div>
                                    </div>
                                    ...
                                </div>
                            </div>
                        )
                        }
                    </code>
                </pre>
            </figure>
            <div className="result">
                <div className="container tiles">
                    <div className="row">
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <figure>
                <figcaption>You can add and remove vertical padding for any column ( just add class "tile" or "no-tile")</figcaption>

            </figure>
            <div className="result mb15">
                <div className="container ">
                    <div className="row">
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3 tile">
                            <div className="column-inner j5"> column .tile inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Container.tiles</h3>
            <div className="result mb15">
                <div className="container tiles">
                    <div className="row">
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3 no-tile">
                            <div className="column-inner j5"> column .no-tile inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                    </div>
                </div>
            </div>
            <p>It's may be usefull if you insert container in container with tiles</p>

            <h2>Grid size</h2>
            <p>Just add class "sm" to container for half-size paddings</p>
            <div className="result mb15">
                <div className="container sm">
                    <div className="row">
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column .tile inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Container.tiles.sm</h3>
            <div className="result mb15">
                <div className="container tiles sm">
                    <div className="row">
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column .no-tile inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                        <div className="col-12 col-lg-3">
                            <div className="column-inner j5"> column inner</div>
                        </div>
                    </div>
                </div>
            </div>

            <h2>Customize size</h2>
            <p>Default size of paddings 15px, but you can custumize it on sass</p>
            <figure>
                <figcaption>Just add global variable $gAir in yous style.scss</figcaption>
                <pre>
                    <code>
                        {<p>
                            $gAir: 20px !global;<br/>
                            @import "anme-vars";<br/>
                            @import "anme-grid";<br/>
                       </p>
                        }
                    </code>
                </pre>
            </figure>
        </div>
    )
}

export default GridSystem